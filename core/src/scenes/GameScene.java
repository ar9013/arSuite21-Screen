package scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.yue.ar.suite.ARSuite;
import player.Player;

public class GameScene implements Screen{

    private ARSuite arSuite;
    private Texture bg;
    private Player player;

    public GameScene(ARSuite arSuite){
        this.arSuite = arSuite;

       // bg = new Texture("bg.jpg");

        player = new Player("badlogic.jpg",Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2);

    }

    // first method called
    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {

        // alpha 1 sea things
        Gdx.gl20.glClearColor(1,0,0,1);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

        arSuite.getBatch().begin();
        //arSuite.getBatch().draw(bg,0,0);
        arSuite.getBatch().draw(player,player.getX(),player.getY());
        arSuite.getBatch().end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
