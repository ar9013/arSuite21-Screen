package com.yue.ar.suite;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import scenes.GameScene;
import scenes.MainMenu;

public class ARSuite extends Game {

	private SpriteBatch batch;
	GameScene gameScene;
	MainMenu mainMenu;

	@Override
	public void create () {
	batch = new SpriteBatch();

	mainMenu = new MainMenu(this);
	gameScene = new GameScene(this);

	setScreen(mainMenu);

	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {

	}

	public SpriteBatch getBatch(){
		super.dispose();
		return this.batch;


	}

	/**
	 * 开始场景展示完毕后调用该方法切换到主游戏场景
	 */
	public void showGameScreen() {
		// 设置当前场景为主游戏场景
		setScreen(mainMenu);

		if (mainMenu != null) {
			// 由于 StartScreen 只有在游戏启动时展示一下, 之后都不需要展示,
			// 所以启动完 GameScreen 后手动调用 StartScreen 的 dispose() 方法销毁开始场景。
			mainMenu.dispose();

			// 场景销毁后, 场景变量值空, 防止二次调用 dispose() 方法
			gameScene = null;
		}
	}

}
